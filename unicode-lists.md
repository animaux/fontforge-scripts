<!-- 

Unicode values for language sets based on fontsquirrel’s webfont-generator 

Converted from actual characters via https://www.branah.com/unicode-converter (UTF16 —> Check: Convert whitespace characters)

Find: \\u(....)
Replace: SelectMoreIf("u\1")\r

-->

--------------------------------------------------------------------------------

# Typo Additions (not from fontsquirrel)

·×»«‹›¹²³¼½¾ﬀﬁﬂﬃﬄ
 
## —> UTF-16

\u00b7\u00d7\u00bb\u00ab\u2039\u203a\u00b9\u00b2\u00b3\u00bc\u00bd\u00be\ufb00\ufb01\ufb02\ufb03\ufb04

--------------------------------------------------------------------------------

# English

!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~¢£¥©®´–—‘’“”•…€™ 
 
## —> UTF-16

\u0021\u0022\u0023\u0024\u0025\u0026\u0027\u0028\u0029\u002a\u002b\u002c\u002d\u002e\u002f\u0030\u0031\u0032\u0033\u0034\u0035\u0036\u0037\u0038\u0039\u003a\u003b\u003c\u003d\u003e\u003f\u0040\u0041\u0042\u0043\u0044\u0045\u0046\u0047\u0048\u0049\u004a\u004b\u004c\u004d\u004e\u004f\u0050\u0051\u0052\u0053\u0054\u0055\u0056\u0057\u0058\u0059\u005a\u005b\u005c\u005d\u005e\u005f\u0060\u0061\u0062\u0063\u0064\u0065\u0066\u0067\u0068\u0069\u006a\u006b\u006c\u006d\u006e\u006f\u0070\u0071\u0072\u0073\u0074\u0075\u0076\u0077\u0078\u0079\u007a\u007b\u007c\u007d\u007e\u00a2\u00a3\u00a5\u00a9\u00ae\u00b4\u2013\u2014\u2018\u2019\u201c\u201d\u2022\u2026\u20ac\u2122\u0020

--------------------------------------------------------------------------------

# German/English

!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~¢£¥¨©«®²³´·¸¹»¼½¾ÄÖ×Üßäöüˆ˚˜–—‘’‚“”„…‹›€™ ̈¨
 
## —> UTF-16

\u0021\u0022\u0023\u0024\u0025\u0026\u0027\u0028\u0029\u002a\u002b\u002c\u002d\u002e\u002f\u0030\u0031\u0032\u0033\u0034\u0035\u0036\u0037\u0038\u0039\u003a\u003b\u003c\u003d\u003e\u003f\u0040\u0041\u0042\u0043\u0044\u0045\u0046\u0047\u0048\u0049\u004a\u004b\u004c\u004d\u004e\u004f\u0050\u0051\u0052\u0053\u0054\u0055\u0056\u0057\u0058\u0059\u005a\u005b\u005c\u005d\u005e\u005f\u0060\u0061\u0062\u0063\u0064\u0065\u0066\u0067\u0068\u0069\u006a\u006b\u006c\u006d\u006e\u006f\u0070\u0071\u0072\u0073\u0074\u0075\u0076\u0077\u0078\u0079\u007a\u007b\u007c\u007d\u007e\u00a2\u00a3\u00a5\u00a8\u00a9\u00ab\u00ae\u00b2\u00b3\u00b4\u00b7\u00b8\u00b9\u00bb\u00bc\u00bd\u00be\u00c4\u00d6\u00d7\u00dc\u00df\u00e4\u00f6\u00fc\u02c6\u02da\u02dc\u2013\u2014\u2018\u2019\u201a\u201c\u201d\u201e\u2026\u2039\u203a\u20ac\u2122\u0020\u0308\u00a8

--------------------------------------------------------------------------------

# German/English + Typo

!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~¢£¥¨©«®²³´·¸¹»¼½¾ÄÖ×Üßäöüˆ˚˜–—‘’‚“”„…‹›€™ ̈·×»«‹›¹²³¼½¾ ̈¨
 
## —> UTF-16

\u0021\u0022\u0023\u0024\u0025\u0026\u0027\u0028\u0029\u002a\u002b\u002c\u002d\u002e\u002f\u0030\u0031\u0032\u0033\u0034\u0035\u0036\u0037\u0038\u0039\u003a\u003b\u003c\u003d\u003e\u003f\u0040\u0041\u0042\u0043\u0044\u0045\u0046\u0047\u0048\u0049\u004a\u004b\u004c\u004d\u004e\u004f\u0050\u0051\u0052\u0053\u0054\u0055\u0056\u0057\u0058\u0059\u005a\u005b\u005c\u005d\u005e\u005f\u0060\u0061\u0062\u0063\u0064\u0065\u0066\u0067\u0068\u0069\u006a\u006b\u006c\u006d\u006e\u006f\u0070\u0071\u0072\u0073\u0074\u0075\u0076\u0077\u0078\u0079\u007a\u007b\u007c\u007d\u007e\u00a2\u00a3\u00a5\u00a8\u00a9\u00ab\u00ae\u00b2\u00b3\u00b4\u00b7\u00b8\u00b9\u00bb\u00bc\u00bd\u00be\u00c4\u00d6\u00d7\u00dc\u00df\u00e4\u00f6\u00fc\u02c6\u02da\u02dc\u2013\u2014\u2018\u2019\u201a\u201c\u201d\u201e\u2026\u2039\u203a\u20ac\u2122\u0020\u00b7\u00d7\u00bb\u00ab\u2039\u203a\u00b9\u00b2\u00b3\u00bc\u00bd\u00be\u0020\ufb00\ufb01\ufb02\ufb03\ufb04\u0308\u00a8

--------------------------------------------------------------------------------

# German/English/French + Typo

!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~¢£¥¨©«®²³´·¸¹»¼½¾ÀÂÄÆÇÈÉÊËÎÏÔÖ×ÙÛÜßàâäæçèéêëîïôöùûüÿŒœŸˆ˚˜–—‘’‚“”„•…‹›€™ ̈¨

## —> UTF-16

\u0021\u0022\u0023\u0024\u0025\u0026\u0027\u0028\u0029\u002a\u002b\u002c\u002d\u002e\u002f\u0030\u0031\u0032\u0033\u0034\u0035\u0036\u0037\u0038\u0039\u003a\u003b\u003c\u003d\u003e\u003f\u0040\u0041\u0042\u0043\u0044\u0045\u0046\u0047\u0048\u0049\u004a\u004b\u004c\u004d\u004e\u004f\u0050\u0051\u0052\u0053\u0054\u0055\u0056\u0057\u0058\u0059\u005a\u005b\u005c\u005d\u005e\u005f\u0060\u0061\u0062\u0063\u0064\u0065\u0066\u0067\u0068\u0069\u006a\u006b\u006c\u006d\u006e\u006f\u0070\u0071\u0072\u0073\u0074\u0075\u0076\u0077\u0078\u0079\u007a\u007b\u007c\u007d\u007e\u00a2\u00a3\u00a5\u00a8\u00a9\u00ab\u00ae\u00b2\u00b3\u00b4\u00b7\u00b8\u00b9\u00bb\u00bc\u00bd\u00be\u00c0\u00c2\u00c4\u00c6\u00c7\u00c8\u00c9\u00ca\u00cb\u00ce\u00cf\u00d4\u00d6\u00d7\u00d9\u00db\u00dc\u00df\u00e0\u00e2\u00e4\u00e6\u00e7\u00e8\u00e9\u00ea\u00eb\u00ee\u00ef\u00f4\u00f6\u00f9\u00fb\u00fc\u00ff\u0152\u0153\u0178\u02c6\u02da\u02dc\u2013\u2014\u2018\u2019\u201a\u201c\u201d\u201e\u2022\u2026\u2039\u203a\u20ac\u2122\u0020\ufb00\ufb01\ufb02\ufb03\ufb04\u0308\u00a8


--------------------------------------------------------------------------------

# French + Typo

·×»«‹›¹²³¼½¾!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~¢£¥¨©«®²³´·¸¹»¼½¾ÀÂÆÇÈÉÊËÎÏÔ×ÙÛÜàâæçèéêëîïôùûüÿŒœŸˆ˚˜–—‘’‚“”„…‹›€™ ̈¨


## —> UTF-16

\u00b7\u00d7\u00bb\u00ab\u2039\u203a\u00b9\u00b2\u00b3\u00bc\u00bd\u00be\u0021\u0022\u0023\u0024\u0025\u0026\u0027\u0028\u0029\u002a\u002b\u002c\u002d\u002e\u002f\u0030\u0031\u0032\u0033\u0034\u0035\u0036\u0037\u0038\u0039\u003a\u003b\u003c\u003d\u003e\u003f\u0040\u0041\u0042\u0043\u0044\u0045\u0046\u0047\u0048\u0049\u004a\u004b\u004c\u004d\u004e\u004f\u0050\u0051\u0052\u0053\u0054\u0055\u0056\u0057\u0058\u0059\u005a\u005b\u005c\u005d\u005e\u005f\u0060\u0061\u0062\u0063\u0064\u0065\u0066\u0067\u0068\u0069\u006a\u006b\u006c\u006d\u006e\u006f\u0070\u0071\u0072\u0073\u0074\u0075\u0076\u0077\u0078\u0079\u007a\u007b\u007c\u007d\u007e\u00a2\u00a3\u00a5\u00a8\u00a9\u00ab\u00ae\u00b2\u00b3\u00b4\u00b7\u00b8\u00b9\u00bb\u00bc\u00bd\u00be\u00c0\u00c2\u00c6\u00c7\u00c8\u00c9\u00ca\u00cb\u00ce\u00cf\u00d4\u00d7\u00d9\u00db\u00dc\u00e0\u00e2\u00e6\u00e7\u00e8\u00e9\u00ea\u00eb\u00ee\u00ef\u00f4\u00f9\u00fb\u00fc\u00ff\u0152\u0153\u0178\u02c6\u02da\u02dc\u2013\u2014\u2018\u2019\u201a\u201c\u201d\u201e\u2026\u2039\u203a\u20ac\u2122\u0020\ufb00\ufb01\ufb02\ufb03\ufb04\u0308\u00a8

--------------------------------------------------------------------------------

# # German/English/French/Spanish + Typo

!"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~¡¢£¥¨©«®²³´·¸¹»¼½¾¿ÀÁÂÄÆÇÈÉÊËÍÎÏÑÓÔÖ×ÙÚÛÜßàáâäæçèéêëíîïñóôöùúûüÿŒœŸˆ˚˜–—‘’‚“”„•…‹›€™̈¨

## —> UTF-16

\u0021\u0022\u0023\u0024\u0025\u0026\u0027\u0028\u0029\u002a\u002b\u002c\u002d\u002e\u002f\u0030\u0031\u0032\u0033\u0034\u0035\u0036\u0037\u0038\u0039\u003a\u003b\u003c\u003d\u003e\u003f\u0040\u0041\u0042\u0043\u0044\u0045\u0046\u0047\u0048\u0049\u004a\u004b\u004c\u004d\u004e\u004f\u0050\u0051\u0052\u0053\u0054\u0055\u0056\u0057\u0058\u0059\u005a\u005b\u005c\u005d\u005e\u005f\u0060\u0061\u0062\u0063\u0064\u0065\u0066\u0067\u0068\u0069\u006a\u006b\u006c\u006d\u006e\u006f\u0070\u0071\u0072\u0073\u0074\u0075\u0076\u0077\u0078\u0079\u007a\u007b\u007c\u007d\u007e\u00a1\u00a2\u00a3\u00a5\u00a8\u00a9\u00ab\u00ae\u00b2\u00b3\u00b4\u00b7\u00b8\u00b9\u00bb\u00bc\u00bd\u00be\u00bf\u00c0\u00c1\u00c2\u00c4\u00c6\u00c7\u00c8\u00c9\u00ca\u00cb\u00cd\u00ce\u00cf\u00d1\u00d3\u00d4\u00d6\u00d7\u00d9\u00da\u00db\u00dc\u00df\u00e0\u00e1\u00e2\u00e4\u00e6\u00e7\u00e8\u00e9\u00ea\u00eb\u00ed\u00ee\u00ef\u00f1\u00f3\u00f4\u00f6\u00f9\u00fa\u00fb\u00fc\u00ff\u0152\u0153\u0178\u02c6\u02da\u02dc\u2013\u2014\u2018\u2019\u201a\u201c\u201d\u201e\u2022\u2026\u2039\u203a\u20ac\u2122\u0308\u00a8