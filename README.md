## Fontforge Scripts for subsetting fonts

**Use at your own risk!** Don’t try them without looking at the code or knowing what you do.

The `subset-*` scripts are intended to subset the currently open font in Fontforge via File/Execute Script…/Call…. Any type of Saving/Exporting can be done afterwards.

To see what glyphs are included see `unicode-lists.md`. You can also use that as a starting point to modify the scripts for your needs.

### Known issues

Lookup-tables are not cleaned up resulting in errors on font-generation and possibly later on? Standard Ligatures are now included.

### Further reading

+ Scripting basics: http://fontforge.github.io/en-US/documentation/scripting/
+ Scripting functions: https://fontforge.github.io/scripting-alpha.html